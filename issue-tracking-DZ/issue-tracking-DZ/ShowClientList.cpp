#include "ShowClientList.h"

#include <iomanip>
#include <iostream>
#include <Windows.h>

using namespace std;

namespace Factorio
{
	bool ShowClientList(ClientStorage& clients)
	{
		if (clients.GetSize() == 0)
		{
			cout << "������ � �������� �����������";
		}
		else
		{
			size_t maximumNameLength = 0;
			size_t maximumAddressLength = 0;

			for (size_t i = 0; i < clients.GetSize(); ++i)
			{
				if (clients[i].organizationName.length() > maximumNameLength)
				{
					maximumNameLength = clients[i].organizationName.length();
				}

				size_t streetNameLength = 0;
				size_t houseNumberLength = 0;
				size_t buildingNumberLength = 0;

				if (clients[i].address.street.length() > streetNameLength)
				{
					streetNameLength = clients[i].address.street.length();
				}

				uint16_t tempHouse = clients[i].address.house;
				while (tempHouse != 0)
				{
					tempHouse /= 10;
					++houseNumberLength;
				}

				uint16_t tempBuilding = clients[i].address.building;
				while (tempBuilding != 0)
				{
					tempBuilding /= 10;
					++buildingNumberLength;
				}

				size_t addressLength = 4 + streetNameLength + 5 + houseNumberLength;
				if (tempBuilding > 0)
				{
					addressLength = addressLength + 7 + buildingNumberLength;
				}

				if (addressLength > maximumAddressLength)
				{
					maximumAddressLength = addressLength;
				}
			}

			if (maximumNameLength < 11)
			{
				maximumNameLength = 11;
			}

			cout << setw(6) << "���";
			cout << "   ";
			cout << setw(maximumNameLength) << "�����������";
			cout << "   ";
			cout << "�����";
			cout << '\n';

			for (size_t i = 0; i < clients.GetSize(); ++i)
			{
				cout << setw(6) << clients[i].code;
				cout << "   ";
				cout << setw(maximumNameLength) << clients[i].organizationName;
				cout << "   ";

				switch (clients[i].address.populationCenter.type)
				{
				case Client::VILLAGE:
					cout << "�. ";
					break;
				case Client::TOWN:
					cout << "���. ";
					break;
				case Client::CITY:
					cout << "�. ";
					break;
				}
				cout << clients[i].address.populationCenter.name;

				cout << ", ��. " << clients[i].address.street << ", �. " << clients[i].address.house;

				if (clients[i].address.building > 0)
				{
					cout << ", ���. " << clients[i].address.building;
				}

				cout << '\n';
			}
		}

		cout << '\n' << '\n' << '\n';
		cout << "������� Esc, ����� �����" << endl;

		bool exit = false;

		HANDLE consoleInput = GetStdHandle(STD_INPUT_HANDLE);
		DWORD word;
		INPUT_RECORD inputRecord;

		ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
		FlushConsoleInputBuffer(consoleInput);

		while (true)
		{
			if (inputRecord.EventType == KEY_EVENT)
			{
				if (inputRecord.Event.KeyEvent.bKeyDown)
				{
					if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
					{
						exit = true;
						if (exit)
						{
							break;
						}
					}
				}
			}
		}

		return exit;
	}
}