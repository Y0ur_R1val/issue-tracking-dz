#ifndef DRIVER_STORAGE_H
#define DRIVER_STORAGE_H

#include "Driver.h"

#include <string>
#include <vector>

using namespace std;

namespace Factorio
{
	class DriverStorage
	{
	public:
		DriverStorage();
		DriverStorage(string path);

		void Write();
		void Read();

		void Add(Driver newDriver);

		size_t GetSize();

		Driver& operator[](size_t index);

	private:
		string m_path{};
		vector<Driver> m_driverList{};
	};
}

#endif // !DRIVER_STORAGE_H