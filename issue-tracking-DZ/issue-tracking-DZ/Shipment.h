#ifndef SHIPMENT_H
#define SHIPMENT_H

#include <iostream>

namespace Factorio
{
	struct Shipment
	{
		size_t code{};
		size_t consignmentNoteCode{};
		size_t driverCode{};
		float cargoWeight{};
		float cargoVolume{};

		struct Value
		{
			size_t rubles{};
			uint16_t kopecks{};
		};

		Value cargoValue{};
	};
}

#endif // !SHIPMENT_H