#ifndef SHOW_CONSIGNMENT_NOTE_LIST_H
#define SHOW_CONSIGNMENT_NOTE_LIST_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	void ShowConsignmentNoteList(ConsignmentNoteStorage& consignmentNotes, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers);
	bool ChooseConsignmentNote(ConsignmentNoteStorage& consignmentNotes, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers);
}

#endif // !SHOW_CONSIGNMENT_NOTE_LIST_H