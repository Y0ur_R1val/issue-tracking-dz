#include "AddClient.h"

#include <ctime>
#include <iostream>

using namespace std;

namespace Factorio
{
	size_t AddClient(ClientStorage& clientStorage)
	{
		srand(time(0));

		Client newClient{};

		bool codeIsUnique = false;
		bool found = false;

		while (!codeIsUnique)
		{
			int code = 100000 + rand() % (999999 - 100000 + 1);
			newClient.code = static_cast<size_t>(code);

			for (size_t i = 0; i < clientStorage.GetSize(); ++i)
			{
				if (newClient.code == clientStorage[i].code)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				codeIsUnique = false;
				found = false;
			}
			else
			{
				codeIsUnique = true;
			}
		}

		found = false;
		bool firstIteration = true;
		bool isUnique = false;

		while (!isUnique)
		{
			if (firstIteration)
			{
				cout << '\n';
				cout << '\n';
				cout << '\n';
			}
			else
			{
				cout << '\n';
				cout << "������ � ������ ������� ��� ����������. ���������� �����" << '\n';
				cout << '\n';
			}

			firstIteration = false;

			cin.ignore();

			cout << "�������� �����������: ";
			getline(cin, newClient.organizationName);

			cout << "����� �����������: " << '\n';

			cout << '\t' << "��� ���������� ������:" << '\n';
			cout << '\t' << '\t' << "1 - ����" << '\n';
			cout << '\t' << '\t' << "2 - ������ ���������� ����" << '\n';
			cout << '\t' << '\t' << "3 - �����" << '\n';
			cout << '\t' << '\t' << "-> ";
			int type{};
			cin >> type;

			switch (type)
			{
			case 1:
				newClient.address.populationCenter.type = Client::VILLAGE;
				break;
			case 2:
				newClient.address.populationCenter.type = Client::TOWN;
				break;
			case 3:
				newClient.address.populationCenter.type = Client::CITY;
				break;
			}

			cin.ignore();

			cout << '\t' << "�������� ���������� ������: ";
			getline(cin, newClient.address.populationCenter.name);

			cout << '\t' << "�����: ";
			getline(cin, newClient.address.street);

			cout << '\t' << "���: ";
			cin >> newClient.address.house;

			cout << '\t' << "��������: ";
			cin >> newClient.address.building;

			for (size_t i = 0; i < clientStorage.GetSize(); ++i)
			{
				if (newClient.organizationName == clientStorage[i].organizationName
					&& newClient.address.populationCenter.type == clientStorage[i].address.populationCenter.type
					&& newClient.address.populationCenter.name == clientStorage[i].address.populationCenter.name
					&& newClient.address.street == clientStorage[i].address.street
					&& newClient.address.house == clientStorage[i].address.house
					&& newClient.address.building == clientStorage[i].address.building)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				isUnique = false;

				cout << "\033[F";
				cout << '\t' << "          ";
				while (newClient.address.building > 0)
				{
					newClient.address.building /= 10;
					cout << ' ';
				}
				cout << ' ';

				cout << "\033[F";
				cout << '\t' << "     ";
				while (newClient.address.house > 0)
				{
					newClient.address.house /= 10;
					cout << ' ';
				}
				cout << ' ';

				cout << "\033[F";
				cout << '\t' << "       ";
				for (size_t i = 0; i < newClient.address.street.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F";
				cout << '\t' << "                             ";
				for (size_t i = 0; i < newClient.address.populationCenter.name.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F";
				cout << '\t' << '\t' << "   ";
				while (type > 0)
				{
					type /= 10;
					cout << ' ';
				}
				cout << ' ';

				cout << "\033[F";
				cout << '\t' << '\t' << "         ";

				cout << "\033[F";
				cout << '\t' << '\t' << "                           ";

				cout << "\033[F";
				cout << '\t' << '\t' << "        ";

				cout << "\033[F";
				cout << '\t' << "                       ";

				cout << "\033[F";
				cout << "                   ";

				cout << "\033[F";
				cout << "                      ";
				for (size_t i = 0; i < newClient.organizationName.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F" << "\033[F" << "\033[F";
			}
			else
			{
				isUnique = true;
				clientStorage.Add(newClient);
			}
		}

		return newClient.code;
	}
}