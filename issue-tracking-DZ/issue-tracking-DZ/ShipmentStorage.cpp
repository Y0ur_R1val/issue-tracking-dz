#include "ShipmentStorage.h"

#include <fstream>

using namespace std;

namespace Factorio
{
	ShipmentStorage::ShipmentStorage()
	{
		m_path = {};
		m_shipmentList = {};
	}

	ShipmentStorage::ShipmentStorage(string path)
	{
		m_path = path;
		Read();
	}

	void ShipmentStorage::Write()
	{
		size_t size = m_shipmentList.size();

		ofstream file(m_path, ios::binary);
		file.write((char*)&size, sizeof(size_t));

		vector<Shipment>::const_iterator it = m_shipmentList.begin();
		while (it != m_shipmentList.end())
		{
			file.write((char*)&it->code, sizeof(size_t));
			file.write((char*)&it->consignmentNoteCode, sizeof(size_t));
			file.write((char*)&it->driverCode, sizeof(size_t));
			file.write((char*)&it->cargoWeight, sizeof(float));
			file.write((char*)&it->cargoVolume, sizeof(float));
			file.write((char*)&it->cargoValue.rubles, sizeof(size_t));
			file.write((char*)&it->cargoValue.kopecks, sizeof(uint16_t));
			++it;
		}

		file.close();
	}

	void ShipmentStorage::Read()
	{
		size_t size{};

		ifstream file(m_path, ios::binary);
		file.read((char*)&size, sizeof(size_t));

		for (size_t i = 0; i < size; ++i)
		{
			Shipment newShipment{};

			file.read((char*)&newShipment.code, sizeof(size_t));
			file.read((char*)&newShipment.consignmentNoteCode, sizeof(size_t));
			file.read((char*)&newShipment.driverCode, sizeof(size_t));
			file.read((char*)&newShipment.cargoWeight, sizeof(float));
			file.read((char*)&newShipment.cargoVolume, sizeof(float));
			file.read((char*)&newShipment.cargoValue.rubles, sizeof(size_t));
			file.read((char*)&newShipment.cargoValue.kopecks, sizeof(uint16_t));

			m_shipmentList.push_back(newShipment);
		}

		file.close();
	}

	void ShipmentStorage::Add(Shipment newShipment)
	{
		m_shipmentList.push_back(newShipment);
		Write();
	}

	size_t ShipmentStorage::GetSize()
	{
		return m_shipmentList.size();
	}

	Shipment& ShipmentStorage::operator[](size_t index)
	{
		return m_shipmentList[index];
	}
}