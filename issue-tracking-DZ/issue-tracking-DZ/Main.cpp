#include "Menu.h"

#include <Windows.h>

using namespace Factorio;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	ClientStorage clientStorage = ClientStorage("Storage/Clients.dat");
	ConsignmentNoteStorage consignmentNoteStorage = ConsignmentNoteStorage("Storage/ConsignmentNotes.dat");
	DriverStorage driverStorage = DriverStorage("Storage/Drivers.dat");
	OperatorStorage operatorStorage = OperatorStorage("Storage/Operators.dat");
	ShipmentStorage shipmentStorage = ShipmentStorage("Storage/Shipments.dat");

	Menu menu = Menu(clientStorage, consignmentNoteStorage, driverStorage, operatorStorage, shipmentStorage);

	menu.Print();

	return 0;
}