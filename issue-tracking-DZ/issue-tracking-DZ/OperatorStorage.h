#ifndef OPERATOR_STORAGE_H
#define OPERATOR_STORAGE_H

#include "Operator.h"

#include <string>
#include <vector>

using namespace std;

namespace Factorio
{
	class OperatorStorage
	{
	public:
		OperatorStorage();
		OperatorStorage(string path);

		void Write();
		void Read();

		void Add(Operator newOperator);

		size_t GetSize();

		Operator& operator[](size_t index);

	private:
		string m_path{};
		vector<Operator> m_operatorList{};
	};
}

#endif // !OPERATOR_STORAGE_H