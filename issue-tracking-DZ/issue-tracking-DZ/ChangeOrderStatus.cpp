#include "ChangeOrderStatus.h"

#include <iostream>

using namespace std;

namespace Factorio
{
	void ChangeOrderStatus(ConsignmentNoteStorage& consignmentNotes)
	{
		bool codeIsFound = false;
		bool found = false;
		bool firstIteration = true;
		size_t code;
		size_t index{};

		while (!found)
		{
			if (firstIteration)
			{
				cout << "��� ���������: ";
			}
			else
			{
				cout << "������������ ��� ���������. ���������� ��� ���: ";
			}

			cin >> code;

			for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
			{
				if (consignmentNotes[i].code == code)
				{
					codeIsFound = true;
					index = i;
					break;
				}
			}

			if (!codeIsFound)
			{
				found = false;

				cout << "\033[F";
				cout << "                                                ";
				while (code > 0)
				{
					code /= 10;
					cout << ' ';
				}
				cout << ' ';
				cout << '\r';
			}
			else
			{
				found = true;
			}
		}

		int option{};

		if (consignmentNotes[index].orderStatus == ConsignmentNote::GENERATED || consignmentNotes[index].orderStatus == ConsignmentNote::SHIPPED)
		{
			cout << '\n';
			cout << "������� �� �����?" << '\n';
			cout << "1) ��" << '\n';
			cout << "0) ���" << '\n';
			cout << "-> ";
			cin >> option;

			if (option == 1)
			{
				consignmentNotes[index].orderStatus = ConsignmentNote::CANCELED;
				consignmentNotes.Write();
			}
			else if (option == 0)
			{
				if (consignmentNotes[index].orderStatus == ConsignmentNote::GENERATED)
				{
					cout << '\n';
					cout << "��������� �� �����?" << '\n';
					cout << "1) ��" << '\n';
					cout << "0) ���" << '\n';
					cout << "-> ";
					cin >> option;

					if (option == 1)
					{
						consignmentNotes[index].orderStatus = ConsignmentNote::SHIPPED;
						consignmentNotes.Write();
					}
					else if (option == 0)
					{
						consignmentNotes[index].orderStatus = ConsignmentNote::GENERATED;
						consignmentNotes.Write();
					}
				}
			}
		}
	}
}