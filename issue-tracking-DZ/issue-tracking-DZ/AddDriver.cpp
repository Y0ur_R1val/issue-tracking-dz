#include "AddDriver.h"

#include <ctime>
#include <iostream>

using namespace std;

namespace Factorio
{
	bool AddDriver(DriverStorage& driverStorage)
	{
		srand(time(0));

		Driver newDriver{};

		bool codeIsUnique = false;
		bool found = false;

		while (!codeIsUnique)
		{
			int code = 100000 + rand() % (999999 - 100000 + 1);
			newDriver.code = static_cast<size_t>(code);

			for (size_t i = 0; i < driverStorage.GetSize(); ++i)
			{
				if (newDriver.code == driverStorage[i].code)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				codeIsUnique = false;
				found = false;
			}
			else
			{
				codeIsUnique = true;
			}
		}

		found = false;
		bool firstIteration = true;
		bool isUnique = false;

		while (!isUnique)
		{
			if (firstIteration)
			{
				cout << '\n';
				cout << '\n';
				cout << '\n';
				cin.ignore();
			}
			else
			{
				cout << '\n';
				cout << "�������� � ������ ������� ��� ����������. ���������� �����" << '\n';
				cout << '\n';
			}

			firstIteration = false;

			cout << "��� ��������: ";
			getline(cin, newDriver.driverName);

			cout << "������ ������: ";
			getline(cin, newDriver.vehicle);

			cout << "�������� ����: ";
			getline(cin, newDriver.licensePlate);

			for (size_t i = 0; i < driverStorage.GetSize(); ++i)
			{
				if (newDriver.driverName == driverStorage[i].driverName
					&& newDriver.vehicle == driverStorage[i].vehicle
					&& newDriver.licensePlate == driverStorage[i].licensePlate)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				isUnique = false;

				cout << "\033[F";
				cout << "               ";
				for (size_t i = 0; i < newDriver.licensePlate.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F";
				cout << "               ";
				for (size_t i = 0; i < newDriver.vehicle.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F";
				cout << "              ";
				for (size_t i = 0; i < newDriver.driverName.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F" << "\033[F" << "\033[F";
			}
			else
			{
				isUnique = true;
				driverStorage.Add(newDriver);
			}
		}

		return true;
	}
}