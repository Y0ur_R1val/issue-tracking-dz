#ifndef CONSIGNMENT_NOTE_STORAGE_H
#define CONSIGNMENT_NOTE_STORAGE_H

#include "ConsignmentNote.h"

#include <string>
#include <vector>

using namespace std;

namespace Factorio
{
	class ConsignmentNoteStorage
	{
	public:
		ConsignmentNoteStorage();
		ConsignmentNoteStorage(string path);

		void Write();
		void Read();

		void Add(ConsignmentNote newConsignmentNote);

		size_t GetSize();

		ConsignmentNote& operator[](size_t index);

	private:
		string m_path{};
		vector<ConsignmentNote> m_consignmentNoteList{};
	};
}

#endif // !CONSIGNMENT_NOTE_STORAGE_H