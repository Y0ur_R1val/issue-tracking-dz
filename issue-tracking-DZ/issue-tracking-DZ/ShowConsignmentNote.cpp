#include "ShowConsignmentNote.h"

#include <iomanip>
#include <iostream>
#include <Windows.h>

using namespace std;

namespace Factorio
{
	bool ShowConsignmentNote(ConsignmentNoteStorage& consignmentNotes, size_t index, bool operatorIsWatching, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers)
	{
		system("cls");

		cout << '\n';
		cout << "                        ";
		cout << "�������-������������ ���������";
		cout << "              ";
		cout << "���: " << consignmentNotes[index].code;
		cout << '\n' << '\n';
		cout << "���������������: ";

		for (size_t i = 0; i < clients.GetSize(); ++i)
		{
			if (clients[i].code == consignmentNotes[index].organizationCode)
			{
				cout << clients[i].organizationName;
				break;
			}
		}

		cout << '\n' << '\n';
		cout << "                                 ";
		cout << "�������� ������";
		cout << "                                 " << '\n';
		cout << " | ";
		cout << "��� ��������";
		cout << " | ";
		cout << "�����, �";
		cout << " | ";
		cout << "�����, �";
		cout << " | ";
		cout << "�����, ���. ���.";
		cout << " | ";
		cout << "      ��������      ";
		cout << " |" << '\n';

		float totalCargoWeight = 0;
		size_t rublesSum = 0;
		uint16_t kopecksSum = 0;
		uint16_t numberOfShipment = 0;

		for (size_t i = 0; i < shipments.GetSize(); ++i)
		{
			if (shipments[i].consignmentNoteCode == consignmentNotes[index].code)
			{
				++numberOfShipment;

				cout << " | ";
				cout << "   " << shipments[i].code << "   ";
				cout << " | ";
				cout << setw(8) << shipments[i].cargoWeight;
				cout << " | ";
				cout << setw(8) << shipments[i].cargoVolume;
				cout << " | ";
				cout << setw(12) << shipments[i].cargoValue.rubles;
				cout << '-';
				if (shipments[i].cargoValue.kopecks < 10)
				{
					cout << '0' << shipments[i].cargoValue.kopecks << ' ';
				}
				else
				{
					cout << shipments[i].cargoValue.kopecks << ' ';
				}
				cout << " | ";

				for (size_t j = 0; j < drivers.GetSize(); ++j)
				{
					if (drivers[j].code == shipments[i].driverCode)
					{
						cout << drivers[j].driverName;

						HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
						SetConsoleCursorPosition(hConsole, { 79, 6 + numberOfShipment });

						cout << " |" << '\n';

						break;
					}
				}

				rublesSum += shipments[i].cargoValue.rubles;
				kopecksSum += shipments[i].cargoValue.kopecks;
				if (kopecksSum >= 100)
				{
					kopecksSum -= 100;
					++rublesSum;
				}

				totalCargoWeight += shipments[i].cargoWeight * 1000;
			}
		}

		cout << '\n';
		cout << " ����� �����: " << totalCargoWeight << " ��";
		cout << '\n';
		if (kopecksSum < 10)
		{
			cout << " ����� � ������: " << rublesSum << " ���. 0" << kopecksSum << " ���.";
		}
		else
		{
			cout << " ����� � ������: " << rublesSum << " ���. " << kopecksSum << " ���.";
		}
		cout << '\n' << '\n' << '\n';

		bool returnableValue = true;

		HANDLE consoleInput = GetStdHandle(STD_INPUT_HANDLE);
		DWORD word;
		INPUT_RECORD inputRecord;

		if (operatorIsWatching)
		{
			cout << "������� Esc, ����� �����" << endl;

			ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
			FlushConsoleInputBuffer(consoleInput);

			while (true)
			{
				if (inputRecord.EventType == KEY_EVENT)
				{
					if (inputRecord.Event.KeyEvent.bKeyDown)
					{
						if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
						{
							returnableValue = false;
							if (!returnableValue)
							{
								break;
							}
						}
					}
				}
			}

			return returnableValue;
		}
		else
		{
			if (consignmentNotes[index].orderStatus == ConsignmentNote::CANCELED
				|| consignmentNotes[index].orderStatus == ConsignmentNote::GENERATED
				|| consignmentNotes[index].orderStatus == ConsignmentNote::COMPLETED)
			{
				cout << '\n' << "������� Esc, ����� �����" << endl;

				ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
				FlushConsoleInputBuffer(consoleInput);

				while (true)
				{
					if (inputRecord.EventType == KEY_EVENT)
					{
						if (inputRecord.Event.KeyEvent.bKeyDown)
						{
							if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
							{
								returnableValue = false;
								if (!returnableValue)
								{
									break;
								}
							}
						}
					}
				}
			}
			else if (consignmentNotes[index].orderStatus == ConsignmentNote::SHIPPED)
			{
				bool paid = false;

				cout << "��������?" << '\n';
				cout << "1 - ��" << '\n';
				cout << "0 - ���" << '\n';
				cout << "-> ";
				cin >> paid;

				if (paid)
				{
					consignmentNotes[index].orderStatus = ConsignmentNote::COMPLETED;
					consignmentNotes.Write();

					cout << '\n';
					cout << "����� �������!";

					cout << '\n' << "������� Esc, ����� �����" << endl;

					ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
					FlushConsoleInputBuffer(consoleInput);

					while (true)
					{
						if (inputRecord.EventType == KEY_EVENT)
						{
							if (inputRecord.Event.KeyEvent.bKeyDown)
							{
								if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
								{
									returnableValue = false;
									if (!returnableValue)
									{
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		return returnableValue;
	}
}