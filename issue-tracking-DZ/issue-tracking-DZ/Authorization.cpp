#include "AddClient.h"
#include "AddConsignmentNote.h"
#include "AddDriver.h"
#include "ChangeOrderStatus.h"
#include "ShowClientList.h"
#include "ShowConsignmentNoteList.h"
#include "ShowDriverList.h"

#include "Authorization.h"

#include <iostream>
#include <string>

using namespace std;

namespace Factorio
{
	size_t LogIn(ClientStorage& clients)
	{
		bool firstIteration = true;
		bool found = false;
		size_t returnableValue{};

		while (!found)
		{
			if (firstIteration)
			{
				cout << '\n';
				cout << '\n';
				cout << '\n';
			}
			else
			{
				cout << '\n';
				cout << "������������ ���. ���������� �����" << '\n';
				cout << '\n';
			}

			firstIteration = false;

			size_t code;
			cout << "���: ";
			cin >> code;

			for (size_t i = 0; i < clients.GetSize(); ++i)
			{
				if (code == clients[i].code)
				{
					found = true;
					returnableValue = clients[i].code;
					break;
				}
			}

			if (!found)
			{
				cout << "\033[F";
				cout << "     ";
				while (code > 0)
				{
					code /= 10;
					cout << ' ';
				}
				cout << ' ';

				cout << "\033[F" << "\033[F" << "\033[F";
			}
		}

		return returnableValue;
	}

	void LogIn(OperatorStorage& operators, ClientStorage& clients, DriverStorage& drivers, ConsignmentNoteStorage& consignmentNotes, ShipmentStorage& shipments)
	{
		bool firstIteration = true;
		bool found = false;

		while (!found)
		{
			if (firstIteration)
			{
				cout << '\n';
				cout << '\n';
				cout << '\n';

				cin.ignore();
			}
			else
			{
				cout << '\n';
				cout << "������������ ����� �/��� ������. ���������� �����" << '\n';
				cout << '\n';
			}

			firstIteration = false;

			string login;
			cout << "�����: ";
			getline(cin, login);

			string password;
			cout << "������: ";
			getline(cin, password);

			for (size_t i = 0; i < operators.GetSize(); ++i)
			{
				if (login == operators[i].login && password == operators[i].password)
				{
					found = true;
					break;
				}
			}

			if (!found)
			{
				cout << "\033[F";
				cout << "        ";
				for (uint16_t i = 0; i < password.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F";
				cout << "       ";
				for (uint16_t i = 0; i < login.length(); ++i)
				{
					cout << ' ';
				}

				cout << "\033[F" << "\033[F" << "\033[F";
			}
		}

		int returnableValue = -1;

		do
		{
			system("cls");

			cout << "1. �������" << '\n';
			cout << "2. ��������" << '\n';
			cout << "3. ���������" << '\n';
			cout << "4. �������� �������" << '\n';
			cout << "5. �������� ��������" << '\n';
			cout << "6. �������� ���������" << '\n';
			cout << "7. �������� ������ ������" << '\n';
			cout << "0. �����" << '\n';
			cout << "-> ";

			int command;
			cin >> command;

			switch (command)
			{
			case 1:
				while (returnableValue != 1)
				{
					system("cls");
					returnableValue = static_cast<int>(ShowClientList(clients));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 2:
				while (returnableValue != 1)
				{
					system("cls");
					returnableValue = static_cast<int>(ShowDriverList(drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 3:
				while (returnableValue != 1)
				{
					returnableValue = static_cast<int>(ChooseConsignmentNote(consignmentNotes, clients, shipments, drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 4:
				cout << '\n';
				returnableValue = static_cast<int>(AddClient(clients));
				break;

			case 5:
				while (returnableValue != 1)
				{
					cout << '\n';
					returnableValue = static_cast<int>(AddDriver(drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 6:
				while (returnableValue != 1)
				{
					cout << '\n';
					returnableValue = static_cast<int>(AddConsignmentNote(consignmentNotes, clients, shipments, drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 7:
				cout << '\n';
				ChangeOrderStatus(consignmentNotes);
				break;

			case 0:
				returnableValue = 0;
				break;
			}
		} while (returnableValue != 0);
	}

	void Register(OperatorStorage& operators, ClientStorage& clients, DriverStorage& drivers, ConsignmentNoteStorage& consignmentNotes, ShipmentStorage& shipments)
	{
		bool firstIteration = true;
		bool found = false;
		bool isUnique = false;
		string login;
		string password;

		while (!isUnique)
		{
			if (firstIteration)
			{
				cout << '\n';
				cout << '\n';
				cout << '\n';

				cin.ignore();
			}
			else
			{
				cout << '\n';
				cout << "�������� � ������ ������� ��� ����������. ���������� �����" << '\n';
				cout << '\n';
			}

			firstIteration = false;

			cout << "�����: ";
			getline(cin, login);

			cout << "������: ";
			getline(cin, password);

			for (size_t i = 0; i < operators.GetSize(); ++i)
			{
				if (login == operators[i].login && password == operators[i].password)
				{
					found = true;
					break;
				}
			}

			if (found)
			{
				isUnique = false;

				cout << "\033[F";
				cout << "        ";
				for (uint16_t i = 0; i < password.length(); ++i)
				{
					cout << ' ';
				}
				cout << "\033[F";
				cout << "       ";
				for (uint16_t i = 0; i < login.length(); ++i)
				{
					cout << ' ';
				}
				cout << "\033[F" << "\033[F" << "\033[F";
			}
			else
			{
				isUnique = true;
			}
		}

		Operator newOperator{};
		newOperator.login = login;
		newOperator.password = password;

		operators.Add(newOperator);

		int returnableValue = -1;

		do
		{
			system("cls");

			cout << "1. �������" << '\n';
			cout << "2. ��������" << '\n';
			cout << "3. ���������" << '\n';
			cout << "4. �������� �������" << '\n';
			cout << "5. �������� ��������" << '\n';
			cout << "6. �������� ���������" << '\n';
			cout << "7. �������� ������ ������" << '\n';
			cout << "0. �����" << '\n';
			cout << "-> ";

			int command;
			cin >> command;

			switch (command)
			{
			case 1:
				while (returnableValue != 1)
				{
					system("cls");
					returnableValue = static_cast<int>(ShowClientList(clients));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 2:
				while (returnableValue != 1)
				{
					system("cls");
					returnableValue = static_cast<int>(ShowDriverList(drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 3:
				while (returnableValue != 1)
				{
					system("cls");
					returnableValue = static_cast<int>(ChooseConsignmentNote(consignmentNotes, clients, shipments, drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 4:
				cout << '\n';
				returnableValue = static_cast<int>(AddClient(clients));
				break;

			case 5:
				while (returnableValue != 1)
				{
					cout << '\n';
					returnableValue = static_cast<int>(AddDriver(drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 6:
				while (returnableValue != 1)
				{
					cout << '\n';
					returnableValue = static_cast<int>(AddConsignmentNote(consignmentNotes, clients, shipments, drivers));
				}

				if (returnableValue == 1)
				{
					returnableValue = -1;
				}

				break;

			case 7:
				cout << '\n';
				ChangeOrderStatus(consignmentNotes);
				break;

			case 0:
				returnableValue = 0;
				break;
			}
		} while (returnableValue != 0);
	}
}