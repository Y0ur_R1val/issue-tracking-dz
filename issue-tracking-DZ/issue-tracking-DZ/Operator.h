#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>

using namespace std;

namespace Factorio
{
	struct Operator
	{
		string login{};
		string password{};
	};
}

#endif // !OPERATOR_H