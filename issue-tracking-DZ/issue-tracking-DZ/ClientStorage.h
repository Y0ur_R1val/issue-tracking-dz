#ifndef CLIENT_STORAGE_H
#define CLIENT_STORAGE_H

#include "Client.h"

#include <string>
#include <vector>

using namespace std;

namespace Factorio
{
	class ClientStorage
	{
	public:
		ClientStorage();
		ClientStorage(string path);

		void Write();
		void Read();

		void Add(Client newClient);

		size_t GetSize();

		Client& operator[](size_t index);

	private:
		string m_path{};
		vector<Client> m_clientList{};
	};
}

#endif // !CLIENT_STORAGE_H