#ifndef ADD_CLIENT_H
#define ADD_CLIENT_H

#include "ClientStorage.h"

namespace Factorio
{
	size_t AddClient(ClientStorage& clientStorage);
}

#endif // !ADD_CLIENT_H