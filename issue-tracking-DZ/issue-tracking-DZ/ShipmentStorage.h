#ifndef SHIPMENT_STORAGE_H
#define SHIPMENT_STORAGE_H

#include "Shipment.h"

#include <string>
#include <vector>

using namespace std;

namespace Factorio
{
	class ShipmentStorage
	{
	public:
		ShipmentStorage();
		ShipmentStorage(string path);

		void Write();
		void Read();

		void Add(Shipment newShipment);

		size_t GetSize();

		Shipment& operator[](size_t index);

	private:
		string m_path{};
		vector<Shipment> m_shipmentList{};
	};
}

#endif // !SHIPMENT_STORAGE_H