#ifndef SHOW_CLIENT_LIST_H
#define SHOW_CLIENT_LIST_H

#include "ClientStorage.h"

namespace Factorio
{
	bool ShowClientList(ClientStorage& clients);
}

#endif // !SHOW_CLIENT_LIST_H