#include "ShowConsignmentNote.h"

#include "ShowConsignmentNoteList.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <Windows.h>

using namespace std;

namespace Factorio
{
	void ShowConsignmentNoteList(ConsignmentNoteStorage& consignmentNotes, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers)
	{
		uint16_t maximumNumberLength = 0;
		for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
		{
			uint16_t numberLength = 0;

			uint16_t numberOfShipments = consignmentNotes[i].numberOfShipments;
			while (numberOfShipments != 0)
			{
				numberOfShipments /= 10;
				++numberLength;
			}

			if (numberLength > maximumNumberLength)
			{
				maximumNumberLength = numberLength;
			}
		}

		cout << "��� ���������";
		cout << "   ";
		cout << "��� �����������";
		cout << "   ";
		cout << "���-�� ��������";
		cout << "   ";
		cout << "������ ������";
		cout << '\n';

		for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
		{
			cout << setw(13) << consignmentNotes[i].code;
			cout << "   ";
			cout << setw(15) << consignmentNotes[i].organizationCode;
			cout << "   ";
			cout << setw(15) << consignmentNotes[i].numberOfShipments;
			cout << "   ";

			switch (consignmentNotes[i].orderStatus)
			{
			case ConsignmentNote::CANCELED:
				cout << setw(13) << "�������";
				break;
			case ConsignmentNote::GENERATED:
				cout << setw(13) << "�����������";
				break;
			case ConsignmentNote::SHIPPED:
				cout << setw(13) << "���������";
				break;
			case ConsignmentNote::COMPLETED:
				cout << setw(13) << "��������";
				break;
			}

			cout << '\n';
		}
	}

	bool ChooseConsignmentNote(ConsignmentNoteStorage& consignmentNotes, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers)
	{
		bool exit = false;

		if (consignmentNotes.GetSize() == 0)
		{
			cout << "������ � ��������� �����������";
			cout << '\n' << '\n' << '\n';
			cout << "������� Esc, ����� �����" << endl;

			HANDLE consoleInput = GetStdHandle(STD_INPUT_HANDLE);
			DWORD word;
			INPUT_RECORD inputRecord;

			ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
			FlushConsoleInputBuffer(consoleInput);

			while (true)
			{
				if (inputRecord.EventType == KEY_EVENT)
				{
					if (inputRecord.Event.KeyEvent.bKeyDown)
					{
						if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
						{
							exit = true;
							if (exit)
							{
								break;
							}
						}
					}
				}
			}
		}
		else
		{
			bool show = true;
			bool found = false;
			int option;
			size_t index{};

			ShowConsignmentNoteList(consignmentNotes, clients, shipments, drivers);

			cout << '\n' << '\n' << '\n';
			cout << "������� ��� ��������� ��� ��������� ��������� ���������� ��� ���� �� ������ ����������:" << '\n';
			cout << "1, ���� ������ ������ - �������" << '\n';
			cout << "2, ���� ������ ������ - �����������" << '\n';
			cout << "3, ���� ������ ������ - ���������" << '\n';
			cout << "4, ���� ������ ������ - ��������" << '\n';
			cout << "��� 0, ���� ������ �����" << '\n';
			cout << "-> ";
			cin >> option;

			ConsignmentNote::Status chosenStatus{};

			if (option > 0 && option <= 4)
			{
				if (option == 1)
				{
					chosenStatus = ConsignmentNote::CANCELED;
				}
				else if (option == 2)
				{
					chosenStatus = ConsignmentNote::GENERATED;
				}
				else if (option == 3)
				{
					chosenStatus = ConsignmentNote::SHIPPED;
				}
				else if (option == 4)
				{
					chosenStatus = ConsignmentNote::COMPLETED;
				}

				ConsignmentNoteStorage filteredConsignmentNoteStorage = ConsignmentNoteStorage("Storage/FilteredConsignmentNotes.dat");

				ofstream fileToClear("Storage/FilteredConsignmentNotes.dat", ios::binary | ios::trunc);
				fileToClear.close();

				for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
				{
					if (consignmentNotes[i].orderStatus == chosenStatus)
					{
						filteredConsignmentNoteStorage.Add(consignmentNotes[i]);
					}
				}

				ShowConsignmentNoteList(filteredConsignmentNoteStorage, clients, shipments, drivers);

				cout << '\n' << '\n' << '\n';
				cout << "������� ��� ��������� ��� ��������� ��������� ���������� ��� 0, ���� ������ �����" << '\n';
				cout << "-> ";
				size_t code;
				cin >> code;

				while (!found)
				{
					for (size_t i = 0; i < filteredConsignmentNoteStorage.GetSize(); ++i)
					{
						if (code == consignmentNotes[i].code)
						{
							found = true;
							index = i;
							break;
						}
					}

					if (code == 0)
					{
						exit = true;
						break;
					}

					if (!found)
					{
						cout << "�������� ���. ���������� ��� ���";
						cout << "\033[F" << "-> ";
					}
				}

				if (found)
				{
					show = ShowConsignmentNote(filteredConsignmentNoteStorage, index, true, clients, shipments, drivers);
				}
			}
			else if (option >= 100000 && option <= 999999)
			{
				while (!found)
				{
					for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
					{
						if (option == static_cast<int>(consignmentNotes[i].code))
						{
							found = true;
							index = i;
							break;
						}
					}

					if (!found)
					{
						cout << "�������� ���. ���������� ��� ���";
						cout << "\033[F" << "-> ";
					}
				}

				if (found)
				{
					show = ShowConsignmentNote(consignmentNotes, index, true, clients, shipments, drivers);
				}
			}
			else if (option == 0)
			{
				exit = true;
			}
		}

		return exit;
	}
}