#include "OperatorStorage.h"

#include <fstream>

using namespace std;

namespace Factorio
{
	OperatorStorage::OperatorStorage()
	{
		m_path = {};
		m_operatorList = {};
	}

	OperatorStorage::OperatorStorage(string path)
	{
		m_path = path;
		Read();
	}

	void OperatorStorage::Write()
	{
		size_t size = m_operatorList.size();
		size_t stringSize{};

		ofstream file(m_path, ios::binary);
		file.write((char*)&size, sizeof(size_t));

		vector<Operator>::const_iterator it = m_operatorList.begin();
		while (it != m_operatorList.end())
		{
			stringSize = it->login.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->login.c_str(), stringSize);

			stringSize = it->password.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->password.c_str(), stringSize);

			++it;
		}

		file.close();
	}

	void OperatorStorage::Read()
	{
		size_t size{};
		size_t stringSize{};

		ifstream file(m_path, ios::binary);
		file.read((char*)&size, sizeof(size_t));

		for (size_t i = 0; i < size; ++i)
		{
			Operator newOperator{};

			file.read((char*)&stringSize, sizeof(size_t));
			char* str = new char[stringSize] {};
			file.read(str, stringSize);
			newOperator.login = str;
			delete[] str;

			file.read((char*)&stringSize, sizeof(size_t));
			str = new char[stringSize] {};
			file.read(str, stringSize);
			newOperator.password = str;
			delete[] str;

			m_operatorList.push_back(newOperator);
		}

		file.close();
	}

	void OperatorStorage::Add(Operator newOperator)
	{
		m_operatorList.push_back(newOperator);
		Write();
	}

	size_t OperatorStorage::GetSize()
	{
		return m_operatorList.size();
	}

	Operator& OperatorStorage::operator[](size_t index)
	{
		return m_operatorList[index];
	}
}