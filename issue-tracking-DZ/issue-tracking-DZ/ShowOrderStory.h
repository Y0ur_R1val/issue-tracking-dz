#ifndef SHOW_ORDER_STORY_H
#define SHOW_ORDER_STORY_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	void ShowOrderStory(ConsignmentNoteStorage& consignmentNotes, size_t code, ShipmentStorage& shipments, DriverStorage& drivers, ClientStorage& clients);
	bool ChooseOrder(ConsignmentNoteStorage& consignmentNotes, size_t code, ShipmentStorage& shipments, DriverStorage& drivers, ClientStorage& clients);
}

#endif // !SHOW_ORDER_STORY_H