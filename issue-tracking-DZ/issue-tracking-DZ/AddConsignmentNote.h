#ifndef ADD_CONSIGNMENT_NOTE_H
#define ADD_CONSIGNMENT_NOTE_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	bool AddConsignmentNote(ConsignmentNoteStorage& consignmentNoteStorage, ClientStorage& clientStorage, ShipmentStorage& shipmentStorage, DriverStorage& driverStorage);
}

#endif // !ADD_CONSIGNMENT_NOTE_H