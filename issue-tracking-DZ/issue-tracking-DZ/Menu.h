#ifndef MENU_H
#define MENU_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "OperatorStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	class Menu
	{
	public:
		Menu(ClientStorage& clientStorage, ConsignmentNoteStorage& consignmentNoteStorage, DriverStorage& driverStorage, OperatorStorage& operatorStorage, ShipmentStorage& shipmentStorage);

		void Print();

	private:
		ClientStorage m_clientStorage{};
		ConsignmentNoteStorage m_consignmentNoteStorage{};
		DriverStorage m_driverStorage{};
		OperatorStorage m_operatorStorage{};
		ShipmentStorage m_shipmentStorage{};
	};
}

#endif // !MENU_H