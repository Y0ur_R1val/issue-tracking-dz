#include "ShowConsignmentNote.h"

#include "ShowOrderStory.h"

#include <iomanip>
#include <iostream>
#include <Windows.h>

using namespace std;

namespace Factorio
{
	void ShowOrderStory(ConsignmentNoteStorage& consignmentNotes, size_t code, ShipmentStorage& shipments, DriverStorage& drivers, ClientStorage& clients)
	{
		cout << '\n';
		cout << "��� ������";
		cout << "   ";
		cout << "�����";
		cout << '\t';
		cout << setw(11) << "������";
		cout << '\n';

		for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
		{
			if (code == consignmentNotes[i].organizationCode)
			{
				cout << setw(10) << consignmentNotes[i].code;
				cout << "   ";

				size_t rubleSum = 0;
				uint16_t kopeckSum = 0;

				for (size_t j = 0; j < shipments.GetSize(); ++j)
				{
					if (shipments[j].consignmentNoteCode == consignmentNotes[i].code)
					{
						rubleSum += shipments[j].cargoValue.rubles;
						kopeckSum += shipments[j].cargoValue.kopecks;

						while (kopeckSum >= 100)
						{
							kopeckSum %= 100;
							++rubleSum;
						}
					}
				}

				cout << rubleSum << ',' << kopeckSum;
				cout << '\t';

				switch (consignmentNotes[i].orderStatus)
				{
				case ConsignmentNote::CANCELED:
					cout << setw(11) << "�������";
					break;
				case ConsignmentNote::GENERATED:
					cout << setw(11) << "�����������";
					break;
				case ConsignmentNote::SHIPPED:
					cout << setw(11) << "���������";
					break;
				case ConsignmentNote::COMPLETED:
					cout << setw(11) << "��������";
					break;
				}
			}
		}
	}

	bool ChooseOrder(ConsignmentNoteStorage& consignmentNotes, size_t code, ShipmentStorage& shipments, DriverStorage& drivers, ClientStorage& clients)
	{
		bool exit = false;

		if (consignmentNotes.GetSize() == 0)
		{
			cout << '\n';
			cout << "���� ������� ������� �����";

			cout << '\n' << '\n' << '\n';
			cout << "������� Esc, ����� ����� �� ��������" << endl;

			HANDLE consoleInput = GetStdHandle(STD_INPUT_HANDLE);
			DWORD word;
			INPUT_RECORD inputRecord;

			ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
			FlushConsoleInputBuffer(consoleInput);

			while (true)
			{
				if (inputRecord.EventType == KEY_EVENT)
				{
					if (inputRecord.Event.KeyEvent.bKeyDown)
					{
						if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
						{
							exit = true;
							if (exit)
							{
								break;
							}
						}
					}
				}
			}
		}
		else
		{
			bool found = false;
			bool show{};
			short index{};
			size_t chosenOrder;

			ShowOrderStory(consignmentNotes, code, shipments, drivers, clients);

			cout << '\n' << '\n' << '\n';
			cout << "������� ��� ������������� ������, ��� 0, ���� ������ ����� �� ��������" << '\n';
			cout << "-> ";
			cin >> chosenOrder;

			if (chosenOrder == 0)
			{
				exit = true;
			}
			else if (chosenOrder >= 100000 && chosenOrder <= 999999)
			{
				while (!found)
				{
					for (size_t i = 0; i < consignmentNotes.GetSize(); ++i)
					{
						if (chosenOrder == consignmentNotes[i].code)
						{
							found = true;
							index = static_cast<short>(i);
							break;
						}
					}

					if (!found)
					{
						cout << "�������� ���. ���������� ��� ���";
						cout << "\033[F" << "-> ";
					}
				}

				if (found)
				{
					show = ShowConsignmentNote(consignmentNotes, index, false, clients, shipments, drivers);
					found = false;
				}
			}
		}

		return exit;
	}
}