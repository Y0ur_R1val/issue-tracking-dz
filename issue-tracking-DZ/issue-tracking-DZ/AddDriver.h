#ifndef ADD_DRIVER_H
#define ADD_DRIVER_H

#include "DriverStorage.h"

namespace Factorio
{
	bool AddDriver(DriverStorage& driverStorage);
}

#endif // !ADD_DRIVER_H