#ifndef CLIENT_H
#define CLIENT_H

#include <string>

using namespace std;

namespace Factorio
{
	struct Client
	{
		size_t code{};
		string organizationName{};

		enum PopulationCenterType
		{
			VILLAGE,
			TOWN,
			CITY
		};

		struct PopulationCenter
		{
			PopulationCenterType type{};
			string name{};
		};

		struct Address
		{
			PopulationCenter populationCenter{};
			string street{};
			uint16_t house{};
			uint16_t building{};
		};

		Address address{};
	};
}

#endif // !CLIENT_H