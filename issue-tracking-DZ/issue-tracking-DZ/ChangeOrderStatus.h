#ifndef CHANGE_ORDER_STATUS_H
#define CHANGE_ORDER_STATUS_H

#include "ConsignmentNoteStorage.h"

namespace Factorio
{
	void ChangeOrderStatus(ConsignmentNoteStorage& consignmentNotes);
}

#endif // !CHANGE_ORDER_STATUS_H