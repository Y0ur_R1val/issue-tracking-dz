#ifndef SHOW_DRIVER_LIST_H
#define SHOW_DRIVER_LIST_H

#include "DriverStorage.h"

namespace Factorio
{
	bool ShowDriverList(DriverStorage& drivers);
}

#endif // !SHOW_DRIVER_LIST_H