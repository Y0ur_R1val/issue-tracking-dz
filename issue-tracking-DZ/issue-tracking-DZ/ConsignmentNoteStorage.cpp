#include "ConsignmentNoteStorage.h"

#include <fstream>

using namespace std;

namespace Factorio
{
	ConsignmentNoteStorage::ConsignmentNoteStorage()
	{
		m_path = {};
		m_consignmentNoteList = {};
	}

	ConsignmentNoteStorage::ConsignmentNoteStorage(string path)
	{
		m_path = path;
		Read();
	}

	void ConsignmentNoteStorage::Write()
	{
		size_t size = m_consignmentNoteList.size();

		ofstream file(m_path, ios::binary);
		file.write((char*)&size, sizeof(size_t));

		vector<ConsignmentNote>::const_iterator it = m_consignmentNoteList.begin();
		while (it != m_consignmentNoteList.end())
		{
			file.write((char*)&it->code, sizeof(size_t));
			file.write((char*)&it->organizationCode, sizeof(size_t));
			file.write((char*)&it->numberOfShipments, sizeof(uint16_t));
			file.write((char*)&it->orderStatus, sizeof(ConsignmentNote::Status));
			++it;
		}

		file.close();
	}

	void ConsignmentNoteStorage::Read()
	{
		size_t size{};

		ifstream file(m_path, ios::binary);
		file.read((char*)&size, sizeof(size_t));

		for (size_t i = 0; i < size; ++i)
		{
			ConsignmentNote newConsignmentNote{};

			file.read((char*)&newConsignmentNote.code, sizeof(size_t));
			file.read((char*)&newConsignmentNote.organizationCode, sizeof(size_t));
			file.read((char*)&newConsignmentNote.numberOfShipments, sizeof(uint16_t));
			file.read((char*)&newConsignmentNote.orderStatus, sizeof(ConsignmentNote::Status));

			m_consignmentNoteList.push_back(newConsignmentNote);
		}

		file.close();
	}

	void ConsignmentNoteStorage::Add(ConsignmentNote newConsignmentNote)
	{
		m_consignmentNoteList.push_back(newConsignmentNote);
		Write();
	}

	size_t ConsignmentNoteStorage::GetSize()
	{
		return m_consignmentNoteList.size();
	}

	ConsignmentNote& ConsignmentNoteStorage::operator[](size_t index)
	{
		return m_consignmentNoteList[index];
	}
}