#ifndef DRIVER_H
#define DRIVER_H

#include <string>

using namespace std;

namespace Factorio
{
	struct Driver
	{
		size_t code{};
		string driverName{};
		string vehicle{};
		string licensePlate{};
	};
}

#endif // !DRIVER_H