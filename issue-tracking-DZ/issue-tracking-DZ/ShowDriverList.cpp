#include "ShowDriverList.h"

#include <iomanip>
#include <iostream>
#include <Windows.h>

using namespace std;

namespace Factorio
{
	bool ShowDriverList(DriverStorage& drivers)
	{
		if (drivers.GetSize() == 0)
		{
			cout << "������ � ��������� �����������";
		}
		else
		{
			size_t maximumDriverNameLength = 0;
			size_t maximumVehicleNameLength = 0;

			for (size_t i = 0; i < drivers.GetSize(); ++i)
			{
				if (drivers[i].driverName.length() > maximumDriverNameLength)
				{
					maximumDriverNameLength = drivers[i].driverName.length();
				}

				if (drivers[i].vehicle.length() > maximumVehicleNameLength)
				{
					maximumVehicleNameLength = drivers[i].vehicle.length();
				}
			}

			if (maximumDriverNameLength < 12)
			{
				maximumDriverNameLength = 12;
			}

			if (maximumVehicleNameLength < 10)
			{
				maximumVehicleNameLength = 10;
			}

			cout << setw(6) << "���";
			cout << "   ";
			cout << setw(maximumDriverNameLength) << "��� ��������";
			cout << "   ";
			cout << setw(maximumVehicleNameLength) << "����������";
			cout << "   ";
			cout << setw(13) << "�������� ����";
			cout << '\n';

			for (size_t i = 0; i < drivers.GetSize(); ++i)
			{
				cout << setw(6) << drivers[i].code;
				cout << "   ";
				cout << setw(maximumDriverNameLength) << drivers[i].driverName;
				cout << "   ";
				cout << setw(maximumVehicleNameLength) << drivers[i].vehicle;
				cout << "   ";
				cout << setw(13) << drivers[i].licensePlate;
				cout << '\n';
			}
		}

		cout << '\n' << '\n' << '\n';
		cout << "������� Esc, ����� �����" << endl;

		bool exit = false;

		HANDLE consoleInput = GetStdHandle(STD_INPUT_HANDLE);
		DWORD word;
		INPUT_RECORD inputRecord;

		ReadConsoleInput(consoleInput, &inputRecord, sizeof(INPUT_RECORD), &word);
		FlushConsoleInputBuffer(consoleInput);

		while (true)
		{
			if (inputRecord.EventType == KEY_EVENT)
			{
				if (inputRecord.Event.KeyEvent.bKeyDown)
				{
					if (inputRecord.Event.KeyEvent.wVirtualKeyCode == VK_ESCAPE)
					{
						exit = true;
						if (exit)
						{
							break;
						}
					}
				}
			}
		}

		return exit;
	}
}