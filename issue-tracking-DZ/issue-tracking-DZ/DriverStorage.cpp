#include "DriverStorage.h"

#include <fstream>

using namespace std;

namespace Factorio
{
	DriverStorage::DriverStorage()
	{
		m_path = {};
		m_driverList = {};
	}

	DriverStorage::DriverStorage(string path)
	{
		m_path = path;
		Read();
	}

	void DriverStorage::Write()
	{
		size_t size = m_driverList.size();
		size_t stringSize{};

		ofstream file(m_path, ios::binary);
		file.write((char*)&size, sizeof(size_t));

		vector<Driver>::const_iterator it = m_driverList.begin();
		while (it != m_driverList.end())
		{
			file.write((char*)&it->code, sizeof(size_t));

			stringSize = it->driverName.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->driverName.c_str(), stringSize);

			stringSize = it->vehicle.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->vehicle.c_str(), stringSize);

			stringSize = it->licensePlate.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->licensePlate.c_str(), stringSize);

			++it;
		}

		file.close();
	}

	void DriverStorage::Read()
	{
		size_t size{};
		size_t stringSize{};

		ifstream file(m_path, ios::binary);
		file.read((char*)&size, sizeof(size_t));

		for (size_t i = 0; i < size; ++i)
		{
			Driver newDriver{};
			file.read((char*)&newDriver.code, sizeof(size_t));

			file.read((char*)&stringSize, sizeof(size_t));
			char* str = new char[stringSize] {};
			file.read(str, stringSize);
			newDriver.driverName = str;
			delete[] str;

			file.read((char*)&stringSize, sizeof(size_t));
			str = new char[stringSize] {};
			file.read(str, stringSize);
			newDriver.vehicle = str;
			delete[] str;

			file.read((char*)&stringSize, sizeof(size_t));
			str = new char[stringSize] {};
			file.read(str, stringSize);
			newDriver.licensePlate = str;
			delete[] str;

			m_driverList.push_back(newDriver);
		}

		file.close();
	}

	void DriverStorage::Add(Driver newDriver)
	{
		m_driverList.push_back(newDriver);
		Write();
	}

	size_t DriverStorage::GetSize()
	{
		return m_driverList.size();
	}

	Driver& DriverStorage::operator[](size_t index)
	{
		return m_driverList[index];
	}
}