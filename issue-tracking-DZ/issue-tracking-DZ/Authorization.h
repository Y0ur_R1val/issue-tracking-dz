#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "OperatorStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	size_t LogIn(ClientStorage& clients);
	void LogIn(OperatorStorage& operators, ClientStorage& clients, DriverStorage& drivers, ConsignmentNoteStorage& consignmentNotes, ShipmentStorage& shipments);
	void Register(OperatorStorage& operators, ClientStorage& clients, DriverStorage& drivers, ConsignmentNoteStorage& consignmentNotes, ShipmentStorage& shipments);
}

#endif // !AUTHORIZATION_H