#include "AddConsignmentNote.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <thread>

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

namespace Factorio
{
	bool AddConsignmentNote(ConsignmentNoteStorage& consignmentNoteStorage, ClientStorage& clientStorage, ShipmentStorage& shipmentStorage, DriverStorage& driverStorage)
	{
		if (clientStorage.GetSize() == 0 || driverStorage.GetSize() == 0)
		{
			if (clientStorage.GetSize() == 0 && driverStorage.GetSize() == 0)
			{
				cout << "��� ������ � �������� � ���������. ���������� ��������� ���������";
			}
			else if (clientStorage.GetSize() == 0 && driverStorage.GetSize() != 0)
			{
				cout << "��� ������ � ��������. ���������� ��������� ���������";
			}
			else if (clientStorage.GetSize() != 0 && driverStorage.GetSize() == 0)
			{
				cout << "��� ������ � ���������. ���������� ��������� ���������";
			}

			for (size_t i = 0; i < 1; ++i)
			{
				sleep_for(nanoseconds(3000000000));
			}
		}
		else
		{
			srand(time(0));

			ConsignmentNote newConsignmentNote{};

			bool codeIsUnique = false;
			bool found = false;

			while (!codeIsUnique)
			{
				int code = 100000 + rand() % (999999 - 100000 + 1);
				newConsignmentNote.code = static_cast<size_t>(code);

				for (size_t i = 0; i < consignmentNoteStorage.GetSize(); ++i)
				{
					if (newConsignmentNote.code == consignmentNoteStorage[i].code)
					{
						found = true;
						break;
					}
				}

				if (found)
				{
					codeIsUnique = false;
					found = false;
				}
				else
				{
					codeIsUnique = true;
				}
			}

			cout << "��� ���������: " << newConsignmentNote.code << '\n';

			bool organizationCodeFound = false;
			bool organizationFound = false;
			bool firstIteration = true;

			while (!organizationFound)
			{
				if (firstIteration)
				{
					cout << "��� �����������: ";
				}
				else
				{
					cout << "�������� ��� �����������. ���������� �����: ";
				}

				firstIteration = false;

				cin >> newConsignmentNote.organizationCode;

				for (size_t i = 0; i < clientStorage.GetSize(); ++i)
				{
					if (newConsignmentNote.organizationCode == clientStorage[i].code)
					{
						organizationCodeFound = true;
						break;
					}
				}

				if (!organizationCodeFound)
				{
					organizationFound = false;

					cout << "\033[F";
					cout << "                                            ";
					while (newConsignmentNote.organizationCode > 0)
					{
						newConsignmentNote.organizationCode /= 10;
						cout << ' ';
					}
					cout << ' ';
					cout << '\r';
				}
				else
				{
					organizationFound = true;
				}
			}

			newConsignmentNote.orderStatus = ConsignmentNote::GENERATED;

			cout << "���������� ��������: ";
			cin >> newConsignmentNote.numberOfShipments;

			for (uint16_t i = 0; i < newConsignmentNote.numberOfShipments; ++i)
			{
				cout << '\n';
				cout << "�������� �" << i + 1 << '\n';

				Shipment newShipment{};

				bool shipmentCodeIsUnique = false;
				bool shipmentFound = false;

				while (!shipmentCodeIsUnique)
				{
					int code = 100000 + rand() % (999999 - 100000 + 1);
					newShipment.code = static_cast<size_t>(code);

					for (size_t i = 0; i < shipmentStorage.GetSize(); ++i)
					{
						if (newShipment.code == shipmentStorage[i].code)
						{
							shipmentFound = true;
							break;
						}
					}

					if (shipmentFound)
					{
						shipmentCodeIsUnique = false;
						shipmentFound = false;
					}
					else
					{
						shipmentCodeIsUnique = true;
					}
				}

				cout << "��� ��������: " << newShipment.code << '\n';

				newShipment.consignmentNoteCode = newConsignmentNote.code;

				firstIteration = true;
				bool driverCodeFound = false;
				bool driverFound = false;

				while (!driverFound)
				{
					if (firstIteration)
					{
						cout << "��� ��������: ";
					}
					else
					{
						cout << "�������� ��� ��������. ���������� �����: ";
					}

					firstIteration = false;

					cin >> newShipment.driverCode;

					for (size_t i = 0; i < driverStorage.GetSize(); ++i)
					{
						if (newShipment.driverCode == driverStorage[i].code)
						{
							driverCodeFound = true;
							break;
						}
					}

					if (!driverCodeFound)
					{
						driverFound = false;

						cout << "\033[F";
						cout << "                                         ";
						while (newShipment.driverCode > 0)
						{
							newShipment.driverCode /= 10;
							cout << ' ';
						}
						cout << ' ';
						cout << '\r';
					}
					else
					{
						driverFound = true;
					}
				}

				cout << "����� ����� (�): ";
				cin >> newShipment.cargoWeight;

				cout << "����� ����� (���. �): ";
				cin >> newShipment.cargoVolume;

				cout << "��������� �������: ";
				cin >> newShipment.cargoValue.rubles >> newShipment.cargoValue.kopecks;

				shipmentStorage.Add(newShipment);
			}

			consignmentNoteStorage.Add(newConsignmentNote);
		}

		return true;
	}
}