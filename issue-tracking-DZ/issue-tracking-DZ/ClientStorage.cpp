#include "ClientStorage.h"

#include <fstream>

using namespace std;

namespace Factorio
{
	ClientStorage::ClientStorage()
	{
		m_path = {};
		m_clientList = {};
	}

	ClientStorage::ClientStorage(string path)
	{
		m_path = path;
		Read();
	}

	void ClientStorage::Write()
	{
		size_t size = m_clientList.size();
		size_t stringSize{};

		ofstream file(m_path, ios::binary);
		file.write((char*)&size, sizeof(size_t));

		vector<Client>::const_iterator it = m_clientList.begin();
		while (it != m_clientList.end())
		{
			file.write((char*)&it->code, sizeof(size_t));

			stringSize = it->organizationName.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->organizationName.c_str(), stringSize);

			file.write((char*)&it->address.populationCenter.type, sizeof(Client::PopulationCenterType));

			stringSize = it->address.populationCenter.name.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->address.populationCenter.name.c_str(), stringSize);

			stringSize = it->address.street.length() + 1;
			file.write((char*)&stringSize, sizeof(size_t));
			file.write(it->address.street.c_str(), stringSize);

			file.write((char*)&it->address.house, sizeof(uint16_t));
			file.write((char*)&it->address.building, sizeof(uint16_t));

			++it;
		}

		file.close();
	}

	void ClientStorage::Read()
	{
		size_t size{};
		size_t stringSize{};

		ifstream file(m_path, ios::binary);
		file.read((char*)&size, sizeof(size_t));

		for (size_t i = 0; i < size; ++i)
		{
			Client newClient{};
			file.read((char*)&newClient.code, sizeof(size_t));

			file.read((char*)&stringSize, sizeof(size_t));
			char* str = new char[stringSize] {};
			file.read(str, stringSize);
			newClient.organizationName = str;
			delete[] str;

			file.read((char*)&newClient.address.populationCenter.type, sizeof(Client::PopulationCenterType));

			file.read((char*)&stringSize, sizeof(size_t));
			str = new char[stringSize] {};
			file.read(str, stringSize);
			newClient.address.populationCenter.name = str;
			delete[] str;

			file.read((char*)&stringSize, sizeof(size_t));
			str = new char[stringSize] {};
			file.read(str, stringSize);
			newClient.address.street = str;
			delete[] str;

			file.read((char*)&newClient.address.house, sizeof(uint16_t));
			file.read((char*)&newClient.address.building, sizeof(uint16_t));

			m_clientList.push_back(newClient);
		}

		file.close();
	}

	void ClientStorage::Add(Client newClient)
	{
		m_clientList.push_back(newClient);
		Write();
	}

	size_t ClientStorage::GetSize()
	{
		return m_clientList.size();
	}

	Client& ClientStorage::operator[](size_t index)
	{
		return m_clientList[index];
	}
}