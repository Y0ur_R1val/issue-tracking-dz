#ifndef SHOW_CONSIGNMENT_NOTE_H
#define SHOW_CONSIGNMENT_NOTE_H

#include "ClientStorage.h"
#include "ConsignmentNoteStorage.h"
#include "DriverStorage.h"
#include "ShipmentStorage.h"

namespace Factorio
{
	bool ShowConsignmentNote(ConsignmentNoteStorage& consignmentNotes, size_t index, bool operatorIsWatching, ClientStorage& clients, ShipmentStorage& shipments, DriverStorage& drivers);
}

#endif // !SHOW_CONSIGNMENT_NOTE_H