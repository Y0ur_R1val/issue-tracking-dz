#ifndef CONSIGNMENT_NOTE_H
#define CONSIGNMENT_NOTE_H

#include <iostream>

namespace Factorio
{
	struct ConsignmentNote
	{
		size_t code{};
		size_t organizationCode{};
		uint16_t numberOfShipments{};

		enum Status
		{
			CANCELED,
			GENERATED,
			SHIPPED,
			COMPLETED
		};

		Status orderStatus{};
	};
}

#endif // !CONSIGNMENT_NOTE_H