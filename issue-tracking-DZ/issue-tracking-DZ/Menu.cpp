#include "AddClient.h"
#include "Authorization.h"
#include "ShowOrderStory.h"

#include "Menu.h"

#include <iostream>

using namespace std;

namespace Factorio
{
	Menu::Menu(ClientStorage& clientStorage, ConsignmentNoteStorage& consignmentNoteStorage, DriverStorage& driverStorage, OperatorStorage& operatorStorage, ShipmentStorage& shipmentStorage)
	{
		m_clientStorage = clientStorage;
		m_consignmentNoteStorage = consignmentNoteStorage;
		m_driverStorage = driverStorage;
		m_operatorStorage = operatorStorage;
		m_shipmentStorage = shipmentStorage;
	}

	void Menu::Print()
	{
		int returnableValue = -1;
		int command = -1;
		size_t code{};

		while (command != 0)
		{
			system("cls");

			cout << "1. ����� ��� ��������" << '\n';
			cout << "2. ����� ��� ������" << '\n';
			cout << "3. ������������������ ��� ��������" << '\n';
			cout << "4. ������������������ ��� ������" << '\n';
			cout << "0. �����" << '\n';
			cout << "-> ";
			cin >> command;

			switch (command)
			{
			case 1:
				cout << '\n';
				LogIn(m_operatorStorage, m_clientStorage, m_driverStorage, m_consignmentNoteStorage, m_shipmentStorage);
				break;
			case 2:
				cout << '\n';
				code = LogIn(m_clientStorage);
				while (returnableValue != 1)
				{
					system("cls");
					cout << "��� ���: " << code << '\n';
					returnableValue = static_cast<int>(ChooseOrder(m_consignmentNoteStorage, code, m_shipmentStorage, m_driverStorage, m_clientStorage));
				}
				break;
			case 3:
				cout << '\n';
				Register(m_operatorStorage, m_clientStorage, m_driverStorage, m_consignmentNoteStorage, m_shipmentStorage);
				break;
			case 4:
				cout << '\n';
				code = AddClient(m_clientStorage);
				while (returnableValue != 1)
				{
					system("cls");
					cout << "��� ���: " << code << '\n';
					returnableValue = static_cast<int>(ChooseOrder(m_consignmentNoteStorage, code, m_shipmentStorage, m_driverStorage, m_clientStorage));
				}
				break;
			case 0:
				cout << '\n';
				cout << "���������� ������" << endl;
				returnableValue = -1;
				break;
			}
		}
	}
}